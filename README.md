Base de donnée utilisée : MattHdb sur le serveur à disposition.
Chaîne de connextion : "Data Source=153.109.124.35;Initial Catalog=MattHdb;Persist Security Info=True;User ID=6231db;Password=Pwd46231.".

Lien git : https://gitlab.com/matthieu.hagenbuch/valaisbooking.git



Etat au 17.12.2020 (2ème rendu) :

- Remarques et éléments manquants du premier rendu corrigés
- Base de donnée complète (Pas de chambres dans les hotels de martigny pour le test d'affichage si aucune chambre disponible
- Globalement l'interface est au plus simple mais je voulais quelque chose de fonctionnel, propre et agréable à utiliser sans faire une oeuvre artistique
- J'ai utilisé le CSS et le HTML souvent manuellement pour comprendre la matière et faire au plus juste, et ai décidé de ne pas copier-coller un template déjà existant


Remarques pour la correction :
- Quelques méthodes sont présentes mais pas utilisées, je ne les ai pas effacées car elles pourraient être utiles pour d'autre fonctions (noté dans les commentaires)
- Il n'y a pas d'images pour les Hotels (non demandé), mais celà pourrait s'implémenter de la même manière que les rooms
- Tous les toString() dans les classes de la DTO on été utilisées pour des tests. Elle ne sont pas forcément à jour et ne sont pas utilisées.
- Pour tester la fonction d'adaptation du prix, l'hotel de Sion (id 4) a plus de 70% de chambres bookées le 26 et 26 décembre
- Le site est entièrement en anglais mais certains descriptions sont en français




********************************************************************************************************************************************************************

Etat au 08.11.2020 (premier rendu) :

- Modèle base de donnée dans son état presque final (quelques changements peuvent survenir lors de la suite du développement).
- Peu de données actuellement, seulement pour des tests de fonctionnalités.
- La fonction search m'a pris un temps phénoménal, la requête SQL n'était pas facile (reste à gérer localisation).
- La fonction de modification du prix est en cours, en tout cas l'idée (que j'aimerai faire valider avant de pousuivre).
- La structure du projet est en place, les quelques méthodes fonctionnelles utilisent la BLL.


