﻿using System;
using System.IO;
using BLL;
using DTO;
using Microsoft.Extensions.Configuration;

namespace ConsoleTest
{
    class Program
    {
        /// Permet de lire un fichier de configuration
        public static IConfiguration Configuration { get; } = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("config.json", optional: true, reloadOnChange: true)
            .Build();

        /// <summary>
        /// Le main est utilisé uniquement pour faire des tests
        /// </summary>
        static void Main(string[] args)
        {           
         
            var roomManager = new RoomManager(Configuration);
            DateTime date1 = new DateTime(2020, 11, 10);
            DateTime date2 = new DateTime(2020, 11, 13);

            var rooms = roomManager.Search(date1,date2);

            foreach (var room in rooms)
            {
                Console.WriteLine(room.ToString());
            }


        }
    }
}
