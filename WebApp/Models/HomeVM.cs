﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class HomeVM
    {
        public List<string> Locations{get;set;}
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string ReservationNb { get; set; }
    }
}
