﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class RoomVM
    {
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public List<Room> Rooms { get; set; }

    }
}
