﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class CancelReservationVM
    {
        public string ReservationNb { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }
}
