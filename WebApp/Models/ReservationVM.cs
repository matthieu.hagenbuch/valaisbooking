﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class ReservationVM
    {
        public int IdRoom { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public decimal Price { get; set; }
    }
}
