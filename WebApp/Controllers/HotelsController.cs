﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class HotelsController : Controller
    {
        private IHotelManager HotelManager { get; }
        public HotelsController(IHotelManager hotelManager) {
            HotelManager = hotelManager;
        }
        // GET: HotelsController
        public ActionResult Index()
        {
            var hotels = HotelManager.GetAll();
            return View(hotels);
        }

        // GET: HotelsController/Details/5
        public ActionResult Details(int id, DateTime dateStart, DateTime dateEnd)
        {
            var hotel = HotelManager.GetHotel(id);
            return View(hotel);
        }       
    }
}
