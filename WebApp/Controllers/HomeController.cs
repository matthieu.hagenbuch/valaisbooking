﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BLL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private IHotelManager HotelManager { get; }
        public HomeController(ILogger<HomeController> logger, IHotelManager hotelManager)
        {
            _logger = logger;
            HotelManager = hotelManager;
        }

        public IActionResult Index(string firstname, string lastname, string reservationNb)
        {
            var vm = new HomeVM { Locations = HotelManager.GetLocations(), Firstname=firstname, Lastname=lastname, ReservationNb=reservationNb };
        
            return View(vm);
        }

        [HttpPost]
        public IActionResult Index(DateTime dateStart, DateTime dateEnd, string location)
        {
            if (dateEnd <= dateStart)
            {               
                return RedirectToAction("Index", "Home");
            }    
            
            return RedirectToAction("Index", "Rooms", new { dateStart = dateStart, dateEnd = dateEnd, location = location });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
