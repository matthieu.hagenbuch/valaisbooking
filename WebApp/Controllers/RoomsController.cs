﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL;
using DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class RoomsController : Controller
    {
        private IRoomManager RoomManager;

        public RoomsController (IRoomManager roomManager)
        {
            RoomManager = roomManager;
        }

        // GET: RoomsController
        public ActionResult Index(DateTime dateStart, DateTime dateEnd, String location)
        {
            var rooms = RoomManager.Search(dateStart, dateEnd, location);

            //ModelState.AddModelError()
            var vm = new RoomVM { DateStart = dateStart, DateEnd = dateEnd, Rooms = rooms};

            return View(vm);
        }
    }
}
