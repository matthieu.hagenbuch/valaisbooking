﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class ReservationController : Controller
    {
        private IReservationManager ReservationManager { get; set; }
        public ReservationController(IReservationManager reservationManager)
        {
            ReservationManager = reservationManager;
        }

        // GET: ReservationController
        public ActionResult Index(int id, DateTime dateStart, DateTime dateEnd, decimal price)
        {
            price = price * (decimal)(dateEnd - dateStart).TotalDays;
            var vm = new ReservationVM { IdRoom = id, DateStart = dateStart.ToString("dd.MM.yyyy"), DateEnd = dateEnd.ToString("dd.MM.yyyy"), Price = price };

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string fname, string lname, string email, ReservationVM vm)
        {
            try
            {
                string reservationNb;

                DateTime DateStart = DateTime.ParseExact(vm.DateStart, "MM/dd/yyyy HH:mm:ss", null);
                DateTime DateEnd = DateTime.ParseExact(vm.DateEnd, "MM/dd/yyyy HH:mm:ss", null);

                reservationNb = ReservationManager.AddReservation(vm.IdRoom, fname, lname, email, DateStart, DateEnd, vm.Price);

                return View("Confirmation", reservationNb);

            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Erreur");
                return View();
            }
        }

        [HttpPost]
        public IActionResult Cancel(string reservationNb, string firstname, string lastname)
        {
            var vm = new CancelReservationVM { ReservationNb = reservationNb, Firstname = firstname, Lastname = lastname };
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Confirmation(string Firstname, string Lastname, string ReservationNb)
        {
            try
            {
                if (ReservationManager.CancelReservation(Firstname, Lastname, ReservationNb))
                    return View("CancelConfirmation");
                //Possible avec Javascript ou bootstrap
                else
                {
                    
                    return RedirectToAction("Index", "Home", new { Firstname = Firstname, Lastname = Lastname, ReservationNb = ReservationNb });
                }
            }
            catch
            {
                return View("Index", "Home");
            }
        }
    }
}
