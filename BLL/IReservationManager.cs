﻿using DTO;
using System;
using System.Collections.Generic;

namespace BLL
{
    public interface IReservationManager
    {
        string AddReservation(int idRoom, string customerFirstname, string customerLastname, string customerEmail, DateTime dateStart, DateTime dateEnd, decimal price);      
        List<Reservation> GetAll();
        Boolean CancelReservation(string firstname, string lastname, string reservationNb);
    }
}