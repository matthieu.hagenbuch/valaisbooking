﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public class RoomTypeManager : IRoomTypeManager
    {
        private IRoomTypeDB RoomTypeDB { get; }

        public RoomTypeManager(IRoomTypeDB roomTypeDB)
        {
            RoomTypeDB = roomTypeDB;
        }

        public RoomType GetRoomType(int idType)
        {
            return RoomTypeDB.GetRoomType(idType);
        }
    }
}
