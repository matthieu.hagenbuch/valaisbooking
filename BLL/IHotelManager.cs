﻿using DTO;
using System.Collections.Generic;

namespace BLL
{
    public interface IHotelManager
    {
        List<string> GetLocations();
        List<Hotel> GetAll();
        Hotel GetHotel(int idHotel);
    }
}