﻿using DAL;
using DTO;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Runtime.ConstrainedExecution;
using System.Text;

namespace BLL
{
    public class RoomManager : IRoomManager
    {
        private const double increase = 1.2;
        private const double increaseTreshold = 0.7;
        private IRoomDB RoomDB { get; }
        private IHotelManager HotelManager { get; }
        private IRoomTypeManager RoomTypeManager { get; }
        public RoomManager(IRoomDB roomDB, IHotelManager hotelManager, IRoomTypeManager roomTypeManager)
        {         
            RoomDB = roomDB;
            HotelManager = hotelManager;
            RoomTypeManager = roomTypeManager;
        }

        /// <summary>
        /// Crée une liste de chambres selon les dates et la location. Les objets Hotel, RoomType et Pictures sont
        /// générés pour chaque chambre. Le prix est adapté grave à la méthode PriceAdaption
        /// </summary>
        /// <returns>Liste des chambres disponibles</returns>
        public List<Room> Search(DateTime dateStart, DateTime dateEnd, string location)
        {
            var rooms = RoomDB.Search(dateStart, dateEnd, location);

            if (rooms is null)
            {
                return rooms;
            }

            foreach(var room in rooms)
            {
                room.Hotel = HotelManager.GetHotel(room.IdHotel);
                room.RoomType = RoomTypeManager.GetRoomType(room.IdType);
                room.Price = room.Price * PriceAdaptation(room.IdHotel, dateStart, dateEnd);
                room.Pictures = RoomDB.GetPicture(room.IdRoom);

                //Si la bdd ne contient aucune image pour une room, une image "coming soon" est mise par défaut.
                if (room.Pictures is null)
                {
                    room.Pictures = new List<string>();
                    room.Pictures.Add("https://www.nopcommerce.com/images/thumbs/0005720_coming-soon-page_550.jpeg");
                }
            }
            return rooms;
        }

        /// <summary>
        /// Calcul l'augmentation tarifaire des nuits si le nombre de taux de réservation dépasse le treshold (70%)
        /// Le méthode fait appel à deux requêtes de RoomDB, GetNbRooms et GetNbBookedRooms.
        /// </summary>
        /// <returns>Augmentation du prix définie par increase</returns>
        public decimal PriceAdaptation (int idHotel, DateTime dateStart, DateTime dateEnd)
        {
            int nbRooms = RoomDB.GetNbRooms(idHotel);
            int nbBookedRooms = RoomDB.GetNbBookedRooms(idHotel, dateStart, dateEnd);
            double bookedRooms = (double)nbBookedRooms / (double)nbRooms;

            if (bookedRooms > increaseTreshold)
                return (decimal)increase;

            return 1;
        }

        //Les deux méthodes suivantes ne sont pas utilisées.
        //Elle peuvent servir à renvoyer une chambre
        //selon son id ou toutes les chambres d'un hotel (pour un affichage des chambres dans les détails
        //d'un hotel par exemple

        public Room GetRoom(int idRoom)
        {
            return RoomDB.GetRoom(idRoom);
        }

        public List<Room> GetAll(int idHotel)
        {
            return RoomDB.GetAll(idHotel);
        }
    }
}