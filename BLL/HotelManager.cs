﻿using DAL;
using DTO;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public class HotelManager : IHotelManager
    {
        private IHotelDB HotelDB { get; }
        public HotelManager(IHotelDB hotelDB)
        {
            HotelDB = hotelDB;
        }

        public List<string> GetLocations()
        {
            return HotelDB.GetLocations();
        }

        public List<Hotel> GetAll()
        {
            return HotelDB.GetAll();
        }

        public Hotel GetHotel(int idHotel)
        {
            return HotelDB.GetHotel(idHotel);
        }

    }
}
