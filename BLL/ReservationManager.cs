﻿using DAL;
using DTO;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public class ReservationManager : IReservationManager
    {
        private IReservationDB ReservationDB;
        public ReservationManager(IReservationDB reservationDB)
        {
            ReservationDB = reservationDB;
        }

        /// <summary>
        /// Génére le numéro de réservation avec 2 premières lettres du nom et prénom + un nombre hexadecimal de 6
        /// caractères. Passe tout en majuscule et ajouter la réservation à la BDD.
        /// Le peut de caractères dans le numéro est pour ne pas avoir quelque chose de trop long. Pour l'infime chance
        /// d'avoir deux numéros identiques, nous pourrions à la place générer un numéro selon l'heure exacte au moment m.
        /// </summary>
        /// <returns>Numéro de réservation</returns>
        public string AddReservation(int idRoom, string customerFirstname, string customerLastname, string customerEmail,
        DateTime dateStart, DateTime dateEnd, decimal price)
        {
            string reservationNumber = customerFirstname.Substring(0, 2) + customerLastname.Substring(0, 2) + Guid.NewGuid().ToString().Substring(0, 6);
            reservationNumber = reservationNumber.ToUpper();

            ReservationDB.AddReservation(idRoom, customerFirstname, customerLastname, customerEmail, reservationNumber, dateStart, dateEnd, price);
                
            return reservationNumber;
        }

        /// <summary>
        /// Efface une réservation selon prénom, nom et numéro
        /// </summary>
        /// <returns>True si l'opération est un succès</returns>
        public Boolean CancelReservation(string firstname, string lastname, string reservationNb)
        {
            Boolean result = false;

            if (ReservationDB.CancelReservation(firstname, lastname, reservationNb) == 1)
                result = true;

            return result;
        }

        /// <summary>
        /// Méthode de test non utilisée
        /// </summary>
        /// <returns>Toutes les réservations</returns>
        public List<Reservation> GetAll()
        {
            return ReservationDB.GetAll();
        }
    }
}