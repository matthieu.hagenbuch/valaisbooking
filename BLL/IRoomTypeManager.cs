﻿using DTO;

namespace BLL
{
    public interface IRoomTypeManager
    {
        RoomType GetRoomType(int idType);
    }
}