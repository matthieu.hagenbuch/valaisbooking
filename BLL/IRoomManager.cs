﻿using DTO;
using System;
using System.Collections.Generic;

namespace BLL
{
    public interface IRoomManager
    {       
        List<Room> Search(DateTime dateStart, DateTime dateEnd, string location);       
        decimal PriceAdaptation(int idHotel, DateTime dateStart, DateTime dateEnd);
        Room GetRoom(int idRoom);
        List<Room> GetAll(int idHotel);
    }
}