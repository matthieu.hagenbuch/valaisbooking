﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class Room
    {
        public int IdRoom { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        //public int Type { get; set; }
        public decimal Price { get; set; }
        public Boolean HasTV { get; set; }
        public Boolean HasHairDryer { get; set; }
        public int IdHotel { get; set; }
        public int IdType { get; set; }
        public Hotel Hotel { get; set; }
        public RoomType RoomType { get; set; }
        public List<string> Pictures { get; set; }


        public override string ToString()
        {
            return " IdRoom: " + IdRoom +
                   " Number: " + Number +
                   " Description: " + Description +
                   " Price : " + Price +
                   " HasTV: " + HasTV +
                   " HasHaireDryer: " + HasHairDryer +
                   " IdType: " + IdType +
                   " IdHotel: " + IdHotel;
        }
    }
}

