﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class Reservation
    {
        public int IdReservation { get; set; }
        public int IdRoom { get; set; }
        public string CustomerFirstname { get; set; }
        public string CustomerLastname { get; set; }
        public string CustomerEmail { get; set; }
        public string ReservationNumber { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public DateTime ReservationDate { get; set; }
        public decimal Price { get; set; }

        public override string ToString()
        {
            return " IdReservation: " + IdReservation +
                   " IdRoom: " + IdRoom +
                   " CustFirstname: " + CustomerFirstname +
                   " CustLastname: " + CustomerLastname +
                   " ReservationNbr: " + ReservationNumber +
                   " DateStart : " + DateStart +
                   " DateEnd: " + DateEnd +
                   " ReservationDate: " + ReservationDate +
                   " PriceAdaptation : " + Price;
        }
    }
}
