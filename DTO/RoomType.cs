﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class RoomType
    {
        public int IdType { get; set; }
        public string TypeName { get; set; }
        public int NbPerson { get; set; }
        public string Description { get; set; }
    }
}
