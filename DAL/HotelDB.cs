﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using DTO;
using Microsoft.Extensions.Configuration;

namespace DAL
{
    public class HotelDB : IHotelDB
    {
        private string connectionString;
        public HotelDB(IConfiguration configuration)
        {
            IConfiguration Configuration = configuration;
            connectionString = Configuration.GetConnectionString("ServerDB");
        }

        public List<string> GetLocations()
        {
            List<string> locations= null;
            try
            {
                using SqlConnection cn = new SqlConnection(connectionString);
                {
                    string query = "Select DISTINCT Location from Hotel";
                    SqlCommand cmd = new SqlCommand(query, cn);

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (locations == null)
                                locations = new List<string>();
                            locations.Add((string)dr["Location"]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return locations;
        }
            public List<Hotel> GetAll()
        {
            List<Hotel> hotels = null;
            try
            {
                using SqlConnection cn = new SqlConnection(connectionString);
                {
                    string query = "Select * from Hotel";
                    SqlCommand cmd = new SqlCommand(query, cn);

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (hotels == null)
                                hotels = new List<Hotel>();

                            Hotel hotel = new Hotel();

                            hotel.IdHotel = (int)dr["idHotel"];
                            hotel.Name = (string)dr["Name"];
                            hotel.Description = (string)dr["Description"];
                            hotel.Location = (string)dr["Location"];
                            hotel.Stars = (int)dr["Stars"];
                            hotel.HasWifi = (Boolean)dr["HasWifi"];
                            hotel.HasParking = (Boolean)dr["HasParking"];
                            if (dr["Phone"] != DBNull.Value)
                                hotel.Phone = (string)dr["Phone"];
                            if (dr["Email"] != DBNull.Value)
                                hotel.Email = (string)dr["Email"];
                            if (dr["Website"] != DBNull.Value)
                                hotel.Website = (string)dr["Website"];

                            hotels.Add(hotel);

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return hotels;
        }

        public Hotel GetHotel(int idHotel)
        {
            Hotel hotel = new Hotel();
            try
            {
                using SqlConnection cn = new SqlConnection(connectionString);
                {
                    string query = "Select * from Hotel Where IdHotel = @IdHotel ";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@IdHotel", idHotel);

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            hotel.IdHotel = (int)dr["IdHotel"];
                            hotel.Name = (string)dr["Name"];
                            hotel.Description = (string)dr["Description"];
                            hotel.Location = (string)dr["Location"];
                            hotel.Stars = (int)dr["Stars"];
                            hotel.HasWifi = (Boolean)dr["HasWifi"];
                            hotel.HasParking = (Boolean)dr["HasParking"];
                            if (dr["Phone"] != DBNull.Value)
                                hotel.Phone = (string)dr["Phone"];
                            if (dr["Email"] != DBNull.Value)
                                hotel.Email = (string)dr["Email"];
                            if (dr["Website"] != DBNull.Value)
                                hotel.Website = (string)dr["Website"];
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return hotel;
        }
    }
}
