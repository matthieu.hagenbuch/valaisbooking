﻿using DTO;
using System;
using System.Collections.Generic;

namespace DAL
{
    public interface IRoomDB
    {
        List<Room> GetAll(int idHotel);
        List<Room> Search(DateTime dateStart, DateTime dateEnd, string location);
        Room GetRoom(int idRoom);
        int GetNbRooms(int idHotel);
        int GetNbBookedRooms(int idHotel, DateTime dateStart, DateTime dateEnd);
        List<string> GetPicture(int idRoom);
    }
}