﻿using Microsoft.Extensions.Configuration;
using DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace DAL
{
    public class ReservationDB : IReservationDB
    {
        private string connectionString;
        public ReservationDB(IConfiguration configuration)
        {
            IConfiguration Configuration = configuration;
            connectionString = configuration.GetConnectionString("ServerDB");
        }

        /// <summary>
        /// Méthode d'ajout de réservation. Données traitées par la BLL
        /// sauf ReservationDate qui sera la date et l'heure actuelle
        /// </summary>
        /// <returns>1 si l'ajout a été effectué</returns>
        public int AddReservation(int idRoom, string customerFirstname, string customerLastname, string customerEmail, string reservationNumber,
            DateTime dateStart, DateTime dateEnd, decimal price)
        {
            try
            {
                using SqlConnection cn = new SqlConnection(connectionString);
                {
                    string query = "insert into Reservation (IdRoom, CustomerFirstname, CustomerLastname, CustomerEmail, ReservationNumber, DateStart," +
                        " DateEnd, ReservationDate, Price) values (@idRoom, @custF, @custL, @custEm, @number, @dateStart, @dateEnd, @date, @price)";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@idRoom", idRoom);
                    cmd.Parameters.AddWithValue("@custF", customerFirstname);
                    cmd.Parameters.AddWithValue("@custL", customerLastname);
                    cmd.Parameters.AddWithValue("@custEm", customerEmail);
                    cmd.Parameters.AddWithValue("@number", reservationNumber);
                    cmd.Parameters.AddWithValue("@dateStart", dateStart);
                    cmd.Parameters.AddWithValue("@dateEnd", dateEnd);
                    cmd.Parameters.AddWithValue("@date", DateTime.Now);
                    cmd.Parameters.AddWithValue("@price", price);

                    cn.Open();

                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Supression d'une réservation avec nom et prénom du client et numéro de réservation
        /// </summary>
        /// <returns>1 si l'annulation a été effectuée</returns>
        public int CancelReservation(string firstname, string lastname, string reservationNb)
        {
            var result = 0;
            try
            {
                using SqlConnection cn = new SqlConnection(connectionString);
                {
                    string query = "DELETE FROM Reservation WHERE CustomerFirstname=@custF AND CustomerLastname=@custL" +
                        " AND ReservationNumber=@reservationNb";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@custF", firstname);
                    cmd.Parameters.AddWithValue("@custL", lastname);
                    cmd.Parameters.AddWithValue("@reservationNb", reservationNb);

                    cn.Open();

                    result = cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return result;
        }

        /// <summary>
        /// Méthode de test, non utilisée dans le programme final
        /// </summary>
        /// <returns>Toutes les réservations</returns>
        public List<Reservation> GetAll()
        {
            List<Reservation> reservations = null;
            try
            {
                using SqlConnection cn = new SqlConnection(connectionString);
                {
                    string query = "Select * from Reservation";
                    SqlCommand cmd = new SqlCommand(query, cn);

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (reservations == null)
                                reservations = new List<Reservation>();

                            Reservation reservation = new Reservation();

                            reservation.IdReservation = (int)dr["IdReservation"];
                            reservation.IdRoom = (int)dr["IdRoom"];
                            reservation.CustomerFirstname = (string)dr["CustomerFirstname"];
                            reservation.CustomerLastname = (string)dr["CustomerLastname"];
                            reservation.CustomerEmail = (string)dr["CustomerEmail"];
                            reservation.ReservationNumber = (string)dr["ReservationNumber"];
                            reservation.DateStart = (DateTime)dr["DateStart"];
                            reservation.DateEnd = (DateTime)dr["DateEnd"];
                            reservation.ReservationDate = (DateTime)dr["ReservationDate"];
                            reservation.Price = (decimal)dr["Price"];

                            reservations.Add(reservation);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return reservations;
        }
    }
}

