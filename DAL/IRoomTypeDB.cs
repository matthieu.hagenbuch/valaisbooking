﻿using DTO;
using System;
using System.Collections.Generic;

namespace DAL
{
    public interface IRoomTypeDB
    {
        RoomType GetRoomType(int idType);
    }
}