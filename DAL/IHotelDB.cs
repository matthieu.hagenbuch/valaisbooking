﻿using DTO;
using System.Collections.Generic;

namespace DAL
{
    public interface IHotelDB
    {
        List<Hotel> GetAll();
        List<string> GetLocations();
        Hotel GetHotel(int idHotel);
    }

}