﻿using DTO;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DAL
{
    public class RoomTypeDB : IRoomTypeDB
    {
        private string connectionString;
        public RoomTypeDB(IConfiguration configuration)
        {
            IConfiguration Configuration = configuration;
            connectionString = configuration.GetConnectionString("ServerDB");
        }
        
        //Note : La méthode GetRoomType aurait pu être écrite directement dans le RoomDB vu que là est
        //son unique utilisation. C'est ce que j'ai fait avec GetPictures.


        public RoomType GetRoomType(int idType)
        {
            RoomType roomType = new RoomType();
            try
            {
                using SqlConnection cn = new SqlConnection(connectionString);
                {
                    string query = "Select * from RoomType Where IdType = @IdType ";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@IdType", idType);

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            roomType.IdType = (int)dr["IdType"];
                            roomType.TypeName = (string)dr["TypeName"];
                            roomType.NbPerson = (int)dr["NumberPersonMax"];

                            if (dr["Description"] != DBNull.Value)
                                roomType.Description = (string)dr["Description"];
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return roomType;
        }
    }
}
