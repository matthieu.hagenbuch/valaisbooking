﻿using Microsoft.Extensions.Configuration;
using DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace DAL
{
    public class RoomDB : IRoomDB
    {
        private string connectionString;
        public RoomDB(IConfiguration configuration)
        {
            IConfiguration Configuration = configuration;
            connectionString = configuration.GetConnectionString("ServerDB");
        }

        /// <summary>
        /// Recherche les chambres valides dans la plage de dates. La requête gère les conflits de date ainsi que les chambres
        /// encore jamais réservées
        /// </summary>
        public List<Room> Search(DateTime dateStart, DateTime dateEnd, String location)
        {
            List<Room> rooms = null;
            try
            {
                using SqlConnection cn = new SqlConnection(connectionString);
                {
                    string query = "SELECT *" +
                                     " FROM Room" +
                                     " WHERE (IdRoom NOT IN (SELECT IdRoom FROM Reservation WHERE @DateEnd >= DateStart AND @DateStart <= DateEnd)" +
                                     " OR IdRoom NOT IN (SELECT IdRoom FROM Reservation))" +
                                     " AND idHotel IN (SELECT idHotel FROM Hotel WHERE Location = @Location)";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@DateStart", dateStart);
                    cmd.Parameters.AddWithValue("@DateEnd", dateEnd);
                    cmd.Parameters.AddWithValue("@Location", location);

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (rooms == null)
                                rooms = new List<Room>();

                            Room room = new Room();

                            room.IdRoom = (int)dr["IdRoom"];
                            room.IdHotel = (int)dr["IdHotel"];
                            room.IdType = (int)dr["IdType"];
                            room.Number = (int)dr["Number"];
                            room.Price = (decimal)dr["Price"];
                            room.HasTV = (Boolean)dr["HasTV"];
                            room.HasHairDryer = (Boolean)dr["HasHairDryer"];
                            if (dr["Description"] != DBNull.Value)
                                room.Description = (string)dr["Description"];

                            rooms.Add(room);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return rooms;
        }

        /// <summary>
        /// Méthode utlisée dans PriceAdaptation
        /// </summary>
        /// <param name="idHotel"></param>
        /// <returns>Nombre total de chambres dans un hotel</returns>
        public int GetNbRooms(int idHotel)
        {
            try
            {
                using SqlConnection cn = new SqlConnection(connectionString);
                {
                    string query = "Select count(*) from Room WHERE idHotel = @IdHotel";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@IdHotel", idHotel);

                    cn.Open();

                    return (int) cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Méthode utlisée dans PriceAdaptation
        /// </summary>
        /// <returns>Nombre total de chambres réservées aux dates données pour un hotel</returns>
        public int GetNbBookedRooms(int idHotel, DateTime dateStart, DateTime dateEnd)
        {
            try
            {
                using SqlConnection cn = new SqlConnection(connectionString);
                {
                    string query = "SELECT count(*) FROM Reservation WHERE idRoom IN(SELECT idRoom FROM Room WHERE idHotel = @IdHotel)" +
                                     " AND NOT(@DateEnd <= DateStart OR @DateStart >= DateEnd); ";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@IdHotel", idHotel);
                    cmd.Parameters.AddWithValue("@DateStart", dateStart);
                    cmd.Parameters.AddWithValue("@DateEnd", dateEnd);

                    cn.Open();

                    return (int)cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Si aucune image dans la BDD, la gestion est faite dans la BLL
        /// </summary>
        /// <param name="idRoom"></param>
        /// <returns>Liste d'images d'une chambre</returns>
        public List<string> GetPicture(int idRoom)
        {
            List<string> urls = null;
            try
            {
                using SqlConnection cn = new SqlConnection(connectionString);
                {
                    string query = "Select * from Picture WHERE IdRoom = @IdRoom";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@IdRoom", idRoom);

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (urls == null)
                                urls = new List<string>();

                            urls.Add((string)dr["Url"]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return urls;
        }


        //Les deux requêtes suivantes ne sont pas utilisées ici mais peuvent être utile pour d'autres fonctions
        public Room GetRoom(int idRoom)
        {
            Room room = new Room();
            try
            {
                using SqlConnection cn = new SqlConnection(connectionString);
                {
                    string query = "Select * from Room WHERE IdRoom = @IdRoom";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@IdRoom", idRoom);

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        room.IdRoom = (int)dr["IdRoom"];
                        room.IdHotel = (int)dr["IdHotel"];
                        room.IdType = (int)dr["IdType"];
                        room.Number = (int)dr["Number"];
                        room.Price = (decimal)dr["Price"];
                        room.HasTV = (Boolean)dr["HasTV"];
                        room.HasHairDryer = (Boolean)dr["HasHairDryer"];
                        if (dr["Description"] != DBNull.Value)
                            room.Description = (string)dr["Description"];
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return room;
        }

        public List<Room> GetAll(int idHotel)
        {
            List<Room> rooms = null;
            try
            {
                using SqlConnection cn = new SqlConnection(connectionString);
                {
                    string query = "Select * from Room WHERE idHotel = @IdHotel";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@IdHotel", idHotel);

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (rooms == null)
                                rooms = new List<Room>();

                            Room room = new Room();

                            room.IdRoom = (int)dr["IdRoom"];
                            room.IdHotel = (int)dr["IdHotel"];
                            room.IdType = (int)dr["IdType"];
                            room.Number = (int)dr["Number"];
                            room.Price = (decimal)dr["Price"];
                            room.HasTV = (Boolean)dr["HasTV"];
                            room.HasHairDryer = (Boolean)dr["HasHairDryer"];
                            if (dr["Description"] != DBNull.Value)
                                room.Description = (string)dr["Description"];

                            rooms.Add(room);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return rooms;
        }
    }
}

