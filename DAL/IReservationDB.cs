﻿using DTO;
using System;
using System.Collections.Generic;

namespace DAL
{
    public interface IReservationDB
    {
        int AddReservation(int idRoom, string customerFirstname, string customerLastname, string customerEmail,
           string reservationNumber, DateTime dateStart, DateTime dateEnd, decimal price);
        int CancelReservation(string firstname, string lastname, string reservationNumber);
        List<Reservation> GetAll();
    }
}